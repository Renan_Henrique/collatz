import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: '',
      sequence: []
    }
  }

  handleChange = event => {
    this.setState({ value: event.target.value })
  }

  handleSubmit = event => {
    let sequence = this.collatzSequence(this.state.value)
    this.setState({ sequence })
    event.preventDefault()
  }

  collatzSequence = (n, store = []) => {
    if (n === 1) {
      store.push(1)
      return store
    } else if (n % 2 === 0) {
      store.push(n)
      return this.collatzSequence(parseInt(n / 2), store)
    } else {
      store.push(n)
      return this.collatzSequence(3 * n + 1, store)
    }
  }

  generateSequenceToDefaultValue = value => {
    let count = 1
    while (value > 1) {
      if (value % 2 === 0) {
        value = value / 2
      } else {
        value = value * 3 + 1
      }
      count++
    }
    return count
  }

  checkToValuesToDefaultValue = () => {
    let maxCount
    let maxSequence = 0

    for (let i = 1; i < 1000000; i++) {
      let sequence = this.generateSequenceToDefaultValue(i)
      if (sequence > maxSequence) {
        maxSequence = sequence
        maxCount = i
      }
    }

    alert(
      `Numero com maior sequência: ${maxCount} \n Sequência obtida: ${maxSequence}`
    )
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <form onSubmit={this.handleSubmit}>
            <input
              type="number"
              value={this.state.value}
              onChange={this.handleChange}
            />
            <input type="submit" value="Check other values" />
            <button
              type=""
              value="1000000"
              onClick={() => this.checkToValuesToDefaultValue()}
            >
              1000000
            </button>
          </form>
        </header>
        <div className="App-footer">
          {this.state.sequence.map((value, index) => {
            if (index === 0)
              return <span className="First-element">{value}, </span>
            return <span>{value}, </span>
          })}
        </div>
      </div>
    )
  }
}

export default App
